use yew::{html, Component, ComponentLink, Html, Renderable, ShouldRender};

use yewstrap::*;

pub struct Navigation {
    is_dropdown_open: bool,
    is_nav_open: bool,
}

pub enum Msg {
    DropdownToggle,
    NavigationToggle,
}

impl Component for Navigation {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        Navigation {
            is_dropdown_open: false,
            is_nav_open: false
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::DropdownToggle => {
                self.is_dropdown_open = !self.is_dropdown_open;
            }
            Msg::NavigationToggle => {
                self.is_nav_open = !self.is_dropdown_open;
            }
        }
        false
    }
}

impl Renderable<Navigation> for Navigation {
    fn view(&self) -> Html<Self> {
        html! {
            <Navbar>
                <Container>
                    <NavbarBrand href="/">{"Yewstrap Navigation"}</NavbarBrand>
                    <NavbarToggler />
                    <Collapse is_open=true navbar=true>
                        <Nav class="ml-auto" navbar=true>
                            <NavItem active=true>
                                <NavLink href="/">
                                    {"Home"}
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink>
                                    {"Example link"}
                                </NavLink>
                            </NavItem>
                            <Dropdown nav=true>
                                <DropdownToggler caret=true nav=true >
                                    {"Account"}
                                </DropdownToggler>
                                <DropdownMenu is_open=true>
                                    <DropdownItem href="https://gitlab.com/gmorzycki">{"My GitLab"}</DropdownItem>
                                    <DropdownDivider />
                                    <DropdownItem>{"Empty"}</DropdownItem>
                                    <DropdownItem>{"Second empty"}</DropdownItem>
                                    <DropdownDivider />
                                    <DropdownItem class="xyz" href="https://google.com">{"Google"}</DropdownItem>
                                </DropdownMenu>
                            </Dropdown>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">{"Disabled"}</a>
                            </li>
                        </Nav>
                    </Collapse>
                </Container>
            </Navbar>
        }
    }
}
