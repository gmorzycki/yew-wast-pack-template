use yew::{html, Component, ComponentLink, Html, Renderable, ShouldRender};
use yewstrap::{Col, Container, Row};

use crate::components::Navigation;

pub struct App {}

pub enum Msg {}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        App {}
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        false
    }
}

impl Renderable<App> for App {
    fn view(&self) -> Html<Self> {
        html! {
            <>
                <Navigation />

                <Container fluid=true>
                    <Row class="xxx">
                    </Row>
                </Container>
                <Container fluid=false>
                    <Row class="xxx">
                        <Col />
                    </Row>
                    <Row class="vvv">
                        <Col class="ttt"/>
                    </Row>
                </Container>

                <Container class="container-main">
                    <Row>
                        <Col>
                            <Col />
                            <h1>{"Test header"}</h1>
                            <p>
                                <h2>{"Second header test"}</h2>
                                {"Test paragraph"}
                                <Container />
                            </p>
                            <Container class="mixed-content"/>
                        </Col>
                    </Row>
                </Container>
            </>

        }
    }
}
